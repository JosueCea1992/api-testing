package whmg.dataprovider;

import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CommentDataProvider {

    @DataProvider
    public Object[][] commentJson() throws IOException {
        String fromJson = new String(Files.readAllBytes(Paths.get("./src/test/resources/data/comment.json")));
        return new Object[][]{{fromJson}};
    }
}
