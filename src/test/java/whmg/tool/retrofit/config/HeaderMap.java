package whmg.tool.retrofit.config;

import java.util.HashMap;
import java.util.Map;

public class HeaderMap {
    private Map<String, String> map;

    public HeaderMap() {
        map = new HashMap<>();
        map.put("Accept", "string");
    }

    /**
     * Return the default HeaderMap ready to be send in a request.
     *
     * @return Map<String, String>
     */
    public Map<String, String> prepareToSend() {
        Map<String, String> map = new HashMap<>();
        map.putAll(this.map);
        map.put("Content-Type", "application/json; charset=utf-8");
        return map;
    }

    /**
     * Return the given HeaderMap ready to be send in a request.
     *
     * @return Map<String, String>
     */
    public Map<String, String> prepareToSend(Map<String, String> mapIn) {
        Map<String, String> mapOut = new HashMap<>();
        mapOut.putAll(mapIn);
        mapOut.put("Content-Type", "application/json; charset=utf-8");
        return mapOut;
    }

    /**
     * Return the default HeaderMap (not ready to send in a request).
     *
     * @return Map<String, String>
     */
    public Map<String, String> getMap() {
        return this.map;
    }
}
