package whmg.tool.retrofit.endpoint;

import retrofit2.Call;
import retrofit2.http.*;

import java.util.Map;

public interface GihubGists {
    @GET("gists/{id}")
    public Call<String> getById(
            @HeaderMap Map<String, String> headers,
            @Path("id") String id);

    @GET("gists/{id}/comments")
    public Call<String> getCommentsById(
            @HeaderMap Map<String, String> headers,
            @Path("id") String id);


    @POST("gists/{id}/comments")
    public Call<String> postComments(
            @HeaderMap Map<String, String> headers,
            @Body String pix,
            @Path("id") String id);


}
