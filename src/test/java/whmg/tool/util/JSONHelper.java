package whmg.tool.util;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.MatcherAssert.assertThat;

public class JSONHelper {

    public static String readFromFile(String path) {
        String content = null;
        if (path != null) {
            try {
                content = new String(Files.readAllBytes(Paths.get(path)));
            } catch (IOException e) {
                if (path.isEmpty()) {
                    path = "empty";
                }
                System.out.println("Schema or Json file could not be found at: " + path);
                //Logger.error("Schema or Json file could not be found at: {}", path, e);
            }
        } else {
            System.out.println("The path to the schema or Json file is null");
            //Logger.error("The path to the schema or Json file is null");
        }
        return content;
    }

    public static boolean validateResponseSchema(String response, String schema) {
        if (response != null && !response.isEmpty() && schema != null && !schema.isEmpty()) {
            assertThat(response, matchesJsonSchema(schema));
        }
        return true;
    }

}
