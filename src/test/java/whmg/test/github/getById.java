package whmg.test.github;

import org.testng.Assert;
import org.testng.annotations.Test;
import retrofit2.Response;
import whmg.tool.retrofit.config.HeaderMap;
import whmg.tool.retrofit.config.ServiceConfig;
import whmg.tool.retrofit.endpoint.GihubGists;
import whmg.tool.util.JSONHelper;

public class getById  {

    @Test(description = "Get response by id 33")
    public void getGistsId() throws Exception {
        GihubGists github = ServiceConfig.retrofit.create(GihubGists.class);
        HeaderMap headerMap = new HeaderMap();
        Response<String> response = github.getById(headerMap.prepareToSend(), "33").execute();
        Assert.assertEquals(response.code(), 200);

        JSONHelper.validateResponseSchema(response.body().toString(),
                JSONHelper.readFromFile("src/test/resources/json_schema/gist_json_schema.json"));

    }

}
