package whmg.test.github;

import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;
import retrofit2.Response;
import whmg.tool.retrofit.config.HeaderMap;
import whmg.tool.retrofit.config.ServiceConfig;
import whmg.tool.retrofit.endpoint.GihubGists;

public class getCommentsById {

    @Test(description = "Get comments using id 33")
    public void getCommentsById() throws Exception {
        GihubGists github = ServiceConfig.retrofit.create(GihubGists.class);
        HeaderMap headerMap = new HeaderMap();
        Response<String> response = github.getCommentsById(headerMap.prepareToSend(), "33").execute();

        SoftAssert softAssert = new SoftAssert();
        softAssert.assertEquals(response.message(), "OK");

        // GET RESPONSE TIME
        long tx = response.raw().sentRequestAtMillis();
        long rx = response.raw().receivedResponseAtMillis();

        long millisResponse = rx-tx;
        boolean rpns500 = false;
        if (millisResponse < 500){
            rpns500 = true;
        }
        softAssert.assertTrue(rpns500);

        System.out.println("response time : "+(rx - tx)+" ms");
        softAssert.assertAll();
    }
}
