package whmg.test.github;

import org.testng.Assert;
import org.testng.annotations.Test;
import retrofit2.Response;
import whmg.dataprovider.CommentDataProvider;
import whmg.tool.retrofit.config.HeaderMap;
import whmg.tool.retrofit.config.ServiceConfig;
import whmg.tool.retrofit.endpoint.GihubGists;
import whmg.tool.util.JSONHelper;

public class postComments {

    @Test(description = "Post comments for id 33", dataProvider = "commentJson", dataProviderClass = CommentDataProvider.class)
    public void postCommentsById(String commentJson) throws Exception {
        String id = "33";
        GihubGists github = ServiceConfig.retrofit.create(GihubGists.class);
        HeaderMap headerMap = new HeaderMap();
        Response<String> response = github.postComments(headerMap.prepareToSend(), commentJson, id ).execute();

        Assert.assertEquals(response.code(), 201);

        JSONHelper.validateResponseSchema(response.body().toString(),
                JSONHelper.readFromFile("src/test/resources/json_schema/comment_json_schema.json"));


    }
}
